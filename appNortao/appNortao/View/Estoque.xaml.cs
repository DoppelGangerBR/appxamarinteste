﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using appNortao.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using appNortao.Interface;
using appNortao.View;
namespace appNortao
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Estoque : ContentPage
	{
        Produto produto = new Produto();
        ProdutoDB IProduto = new ProdutoDB();
        public Estoque ()
		{
			InitializeComponent ();
            listViewProduto.ItemsSource = IProduto.GetProdutos();
            
		}

        private async void atualizaEstoque(object sender, EventArgs e)
        {
            HttpClient _client = new HttpClient();
            JObject jsonObject = new JObject();
            Uri uri = new Uri("http://192.168.0.53:8089/api/produto");
            
            
            try
            {
                //var content = new StringContent("",Encoding.UTF8, "application/json");
                HttpResponseMessage response = _client.GetAsync(uri).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    int quantidadeProdutos = 0;
                    int quantidadeAdicionada = 0;
                    int quantidadeAtualizada = 0;
                    var res = await response.Content.ReadAsStringAsync();
                    dynamic prods = JsonConvert.DeserializeObject(res);
                    
                    foreach(var prod in prods)
                    {
                        produto.CodProduto = prod.codproduto;
                        produto.Discriminacao = prod.nome;
                        produto.Quantidade = prod.quantidade;
                        produto.PrecoUnitario = prod.precovenda;
                        Produto jaExiste = IProduto.GetProduto(produto.CodProduto);
                        if (jaExiste == null)
                        {
                            IProduto.InsereProduto(produto);
                            quantidadeAdicionada++;
                        }
                        else
                        {
                            IProduto.AtualizaProduto(produto);
                            quantidadeAtualizada++;
                        }
                        quantidadeProdutos++;
                    }
                    listViewProduto.ItemsSource = IProduto.GetProdutos();
                    await DisplayAlert("Sucesso", "Estoque Atualizado\n" + quantidadeAdicionada + " Produtos adicionados\n" +
                        quantidadeAtualizada+" Atualizados\n" +
                        "Total de produtos: "+quantidadeProdutos, "OK");

                    /*jsonObject = JObject.Parse(res);
                    produto.CodProduto = Int32.Parse(jsonObject["codproduto"].ToString());
                    produto.Discriminacao = jsonObject["nome"].ToString();
                    produto.Quantidade = Int32.Parse(jsonObject["quantidade"].ToString());*/

                }
                else
                {
                    await DisplayAlert("Falha", "Falha ao atualizar o estoque\nStatus HTTP diferente de 200", "OK");
                }

            }
            catch (Exception ex)
            {
                await DisplayAlert("ERRO","Impossivel atualizar o estoque, por favor, certifique-se de que você esta conectado a internet!\nErro-> "+ex.Message,"OK");
            }
        }

        private void abreProdutoSelecionadoLista(object sender, SelectedItemChangedEventArgs e)
        {
            dynamic produto = listViewProduto.SelectedItem;
            Navigation.PushAsync(new DescricaoProduto(produto));
        }
    }
}