﻿using appNortao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace appNortao.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DescricaoProduto : ContentPage
	{
		public DescricaoProduto (Produto produto)
		{
			InitializeComponent ();
            txtNomeProduto.Text = produto.Discriminacao;
            txtQuantidade.Text = produto.Quantidade.ToString();
            txtPrecoVenda.Text = produto.PrecoUnitario.ToString();
            txtCodProduto.Text = produto.CodProduto.ToString();
        }
        
	}
}