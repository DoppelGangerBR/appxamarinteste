﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net.Http;
using appNortao.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using appNortao.Controller;

namespace appNortao
{
    public partial class MainPage : ContentPage
    {
        private Logs logs;
        public MainPage()
        {

            InitializeComponent();
            using(var dados = new UsuarioDB())
            {
                listaUsuario.ItemsSource = dados.GetUsuarios();
            }
            btnEnviar.Clicked += enviaPost;
        }

        //https://forums.xamarin.com/discussion/86331/how-to-do-post-request-to-a-web-service-php
        private async void enviaPost(object sender, EventArgs e)
        {
            HttpClient _client = new HttpClient();
            var uri = new Uri("http://192.168.0.53:8089/api/Usuario");
            Usuario usuario = new Usuario();
            usuario.usuario = txtUsuario.Text;
            usuario.senha = txtSenha.Text;

            string json = JsonConvert.SerializeObject(usuario);
            try
            {
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = _client.PostAsync(uri, content).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var res = await response.Content.ReadAsStringAsync();
                    JObject jsonObject = JObject.Parse(res);
                    //await DisplayAlert("OK", jsonObject["token_acesso"].ToString(), "OK");
                    await DisplayAlert("OK", jsonObject.ToString(), "OK");
                }
                else
                {
                    await DisplayAlert("OK", "Sem conexão, gravando dados na base", "OK");
                    usuario.usuario = txtUsuario.Text;
                    usuario.senha = txtSenha.Text;
                    using (var dados = new UsuarioDB())
                    {
                        dados.InsereUsuario(usuario);
                        this.listaUsuario.ItemsSource = dados.GetUsuarios();
                    }
                    await DisplayAlert("OK", "DADOS GRAVADOS COM SUCESSO!", "OK");
                }
            }catch (Exception nullException){
                logs.EscreveLog("Erro ao enviar " + nullException.Message);
                Console.WriteLine("ERRO -> " + nullException.Message);
                await DisplayAlert("OK", "Sem conexão, gravando dados na base", "OK");
                usuario.usuario = txtUsuario.Text;
                usuario.senha = txtSenha.Text;
                using (var dados = new UsuarioDB())
                {
                    dados.InsereUsuario(usuario);
                    this.listaUsuario.ItemsSource = dados.GetUsuarios();
                }
                await DisplayAlert("OK", "DADOS GRAVADOS COM SUCESSO!", "OK");
            }
        }

        private async void BtnGravaDados_Clicked(object sender, EventArgs e)
        {
            await DisplayAlert("OK", "Inserindo usuario", "OK");            
                var usuario = new Usuario
                {
                    usuario = this.txtUsuario.Text,
                    senha = this.txtSenha.Text,
                };
                using (var dados = new UsuarioDB())
                {
                    dados.InsereUsuario(usuario);
                    this.listaUsuario.ItemsSource = dados.GetUsuarios();
                }
            await DisplayAlert("OK", "usuario Inserido", "OK");
        }

        private void fazSelect(object sender, EventArgs e)
        {
            using(var dados = new UsuarioDB())
            {
                Usuario usuario = new Usuario();
                usuario = dados.GetUsuario(Int32.Parse(txtId.Text));
                try
                {
                    DisplayAlert("OK", usuario.usuario+'\n'+usuario.senha, "Ok");
                }catch(Exception ex)
                {
                    DisplayAlert("OK", "Nenhum registro encontrado", "Ok");
                }
            }
        }

        private void atualizaDados(object sender, EventArgs e)
        {
            using (var dados = new UsuarioDB())
            {
                Usuario usuario = new Usuario();
                usuario.usuario = txtUsuario.Text;
                usuario.senha = txtSenha.Text;
                dados.AtualizaUsuario(usuario);
                try
                {
                    DisplayAlert("OK", "Usuario Atualizado", "Ok");
                }
                catch (Exception ex)
                {
                    DisplayAlert("OK", "Nenhum registro encontrado", "Ok");
                }

            }
        }

        private void deleta(object sender, EventArgs e)
        {
            using (var dados = new UsuarioDB())
            {
                Usuario usuario = new Usuario();
                usuario.usuario = txtUsuario.Text;
                usuario.senha = txtSenha.Text;
                dados.DeletaUsuario(usuario);
                try
                {
                    DisplayAlert("OK", "Usuario excluido", "Ok");
                }
                catch (Exception ex)
                {
                    DisplayAlert("OK", "Nenhum registro encontrado", "Ok");
                }
            }
        }
        private async Task reenviaAsync()
        {
            using (var dados = new UsuarioDB())
            {
                Usuario usuario = new Usuario();
                usuario = dados.GetUsuario(Int32.Parse(txtId.Text));
                HttpClient _client = new HttpClient();
                var uri = new Uri("http://192.168.0.53:8089/api/Usuario");
                string json = JsonConvert.SerializeObject(usuario);
                try{
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = _client.PostAsync(uri, content).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var res = await response.Content.ReadAsStringAsync();
                        JObject jsonObject = JObject.Parse(res);
                        //await DisplayAlert("OK", jsonObject["token_acesso"].ToString(), "OK");
                        await DisplayAlert("OK", "Reenviado com sucesso!", "OK");
                    }
                }
                catch (Exception ex)
                {
                    await DisplayAlert("Sem Conexão", "Sem conexão com o servidor\nPor favor, tente novamente", "OK");
                }
            }
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await reenviaAsync();
        }
    }
}
