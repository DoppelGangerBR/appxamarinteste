﻿using appNortao.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace appNortao
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Menu : MasterDetailPage
	{
		public Menu ()
		{
			InitializeComponent ();
            Detail = new NavigationPage(new MenuDetail());
		}

        private void abreTelaEstoque(object sender, EventArgs e)
        {
            Detail.Navigation.PushAsync(new Estoque());
            IsPresented = false;
        }

        private void abreTelaVenda(object sender, EventArgs e)
        {
            Detail.Navigation.PushAsync(new Venda());
            IsPresented = false;
        }

        private void ViewCell_Tapped_2(object sender, EventArgs e)
        {
            Detail.Navigation.PushAsync(new Login());
            IsPresented = false;
        }
    }
}