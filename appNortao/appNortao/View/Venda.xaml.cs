﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using appNortao.Interface;
using appNortao.Model;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading;
using Android.Widget;
using System.ComponentModel;

namespace appNortao.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Venda : ContentPage, INotifyPropertyChanged
    {
        public Venda()
        {
            InitializeComponent();
            Produto produto = new Produto();
            ProdutoDB IProduto = new ProdutoDB();
            Cliente cliente = new Cliente();
            ClienteDB ICliente = new ClienteDB();
            listaProdutos.ItemsSource = IProduto.GetProdutos();
            listaClientes.ItemsSource = ICliente.GetClientes();
        }


        public async Task atualizaClienteAsync() {
            HttpClient _client = new HttpClient();
            Uri uri = new Uri("http://192.168.0.53:8089/api/clientes");
            try {
                //var content = new StringContent("",Encoding.UTF8, "application/json");
                HttpResponseMessage response = _client.GetAsync(uri).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK) {
                    var res = await response.Content.ReadAsStringAsync();
                    dynamic _clientes = JsonConvert.DeserializeObject(res);
                    Cliente cliente = new Cliente();
                    ClienteDB ICliente = new ClienteDB();
                    foreach (var _cliente in _clientes) {
                        cliente.CodCliente = _cliente.codcliente;
                        cliente.Endereco = _cliente.endereco;
                        cliente.Cidade = _cliente.cidade;
                        cliente.Celular = _cliente.celular;
                        cliente.CEP = _cliente.cep;
                        cliente.Nome = _cliente.nome;
                        cliente.NomeFantasia = _cliente.nomefantasia;
                        cliente.Numero = _cliente.numero;
                        cliente.Observacao = _cliente.observacao;
                        cliente.Observacao2 = _cliente.observacao2;
                        cliente.RG = _cliente.rg;
                        cliente.Telefone = _cliente.telefone;
                        Cliente jaExiste = ICliente.GetCliente(cliente.CodCliente);
                        if (jaExiste == null) {
                            ICliente.InsereCliente(cliente);
                        }
                        else {
                            ICliente.AtualizaCliente(cliente);
                        }
                    }
                    listaClientes.ItemsSource = ICliente.GetClientes();

                    /*jsonObject = JObject.Parse(res);
                    produto.CodProduto = Int32.Parse(jsonObject["codproduto"].ToString());
                    produto.Discriminacao = jsonObject["nome"].ToString();
                    produto.Quantidade = Int32.Parse(jsonObject["quantidade"].ToString());*/

                }
                else {
                    DisplayAlert("Falha", "Falha ao atualizar os clientes\nStatus HTTP diferente de 200", "OK");
                }

            }
            catch (Exception ex) {
                DisplayAlert("ERRO", "Impossivel atualizar os clientes, por favor, certifique-se de que você esta conectado a internet!\nErro-> " + ex.Message, "OK");
            }
        }
         

        private void buscaProduto(object sender, EventArgs e) {
            ProdutoDB IProduto = new ProdutoDB();
            string nomeBuscado = txtBuscaProduto.Text;
            if (String.IsNullOrEmpty(nomeBuscado)) {
                listaProdutos.ItemsSource = IProduto.GetProdutos();
            }
            else {
                List<Produto> teste = IProduto.GetProdutoPorNome(nomeBuscado).Where<Produto>(produto => produto.Discriminacao == nomeBuscado).ToList();
                teste.ForEach(i => Console.WriteLine("LISTAAA -> " + i.ToString()));
                teste.ForEach(i => DisplayAlert("Teste", "aa" + i.ToString(), "OK"));
                listaProdutos.ItemsSource = IProduto.GetProdutoPorNome(nomeBuscado).Where<Produto>(produto => produto.Discriminacao == nomeBuscado).ToList();
            }
        }

        private async void AtualizaCliente(object sender, EventArgs e) {
            try {
                CarregandoClientes.IsVisible = true;
                CarregandoClientes.IsRunning = true;
                lblActivityIndicator.IsVisible = true;
                Task.Run(() => {
                    atualizaClienteAsync();
                    Device.BeginInvokeOnMainThread(async () => {
                        CarregandoClientes.IsVisible = false;
                        CarregandoClientes.IsRunning = false;
                        lblActivityIndicator.IsVisible = false;
                    });
                });
                
            }
            catch(Exception ex) {
                await DisplayAlert("ERRO", ""+ex, "OK");
            }
        }
    }
}