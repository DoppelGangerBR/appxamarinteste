﻿using appNortao.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace appNortao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        private UsuarioDB IUsuario = new UsuarioDB();
        public Login()
        {
            InitializeComponent();
            //btnLogin.Clicked += fazLogin;
        }

        private async void fazLogin(object sender, EventArgs e)
        {
            HttpClient _client = new HttpClient();
            var uri = new Uri("http://192.168.0.53:8089/api/auth/login");
            Usuario usuario = new Usuario();
            usuario.usuario = txtUsuario.Text;
            usuario.senha = txtSenha.Text;
            HttpResponseMessage response;
            string json = JsonConvert.SerializeObject(usuario);
            try
            {
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                response = _client.PostAsync(uri, content).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    //var res = await response.Content.ReadAsStringAsync();
                    Usuario jaExiste = IUsuario.GetUsuarioLogin(usuario.usuario, usuario.senha);
                    if (jaExiste == null)
                    {
                        IUsuario.InsereUsuario(usuario);
                    }
                    else
                    {
                        IUsuario.AtualizaUsuario(usuario);
                    }
                    Application.Current.MainPage = new Menu();
                }
                else if(response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    var res = await response.Content.ReadAsStringAsync();
                    JObject jsonObject = JObject.Parse(res);
                    await DisplayAlert("", jsonObject["Erro"].ToString(), "OK");
                }else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    var res = await response.Content.ReadAsStringAsync();
                    JObject jsonObject = JObject.Parse(res);
                    await DisplayAlert("", jsonObject["Erro"].ToString(), "OK");
                }
                
            }
            catch (Exception nullException)
            {
                Usuario existente = new Usuario();
                existente = IUsuario.GetUsuarioLogin(usuario.usuario, usuario.senha);
                if(existente == null)
                {
                    await DisplayAlert("Offline", "Você esta offline, conecte-se a internet para continuar", "OK");
                }
                else
                {
                    await DisplayAlert("Offline", "Modo Offline", "OK");
                    Application.Current.MainPage = new Menu();
                }
            }
        }

        private void avancaProximoCampo(object sender, EventArgs e)
        {
            Console.WriteLine("SENDER -> " + sender);
        }
    }
}