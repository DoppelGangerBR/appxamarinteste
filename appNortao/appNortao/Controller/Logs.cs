﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;

namespace appNortao.Controller
{
    class Logs
    {
        public void EscreveLog(String mensagem)
        {
            Console.WriteLine("AAAAAAAAAAAAAAAAAAAAA");
            try
            {
                var config = DependencyService.Get<IConfig>();
                string arquivo = Path.Combine(config.DiretorioSQLite, "log.txt");
                Console.WriteLine("Arquivo -> " + arquivo);
                if (!File.Exists(arquivo)){
                    File.Create(arquivo);
                }
                File.AppendAllText(arquivo, mensagem);
            }
            catch(Exception e)
            {
                Console.WriteLine("AAA Erro -> " + e.Message);
            }
            
        }
    }
}
