﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace appNortao.Model
{
    public class Cliente{
        [PrimaryKey]
        public int CodCliente { get; set; }
        public string Nome { get; set; }
        public string NomeFantasia { get; set; }
        public string Endereco { get; set; }
        public string CEP { get; set; }
        public string Numero { get; set; }
        public string Cidade { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Cnpj_cpf { get; set; }
        public string RG { get; set; }
        public string Observacao { get; set; }
        public string Observacao2 { get; set; }
    }
}
