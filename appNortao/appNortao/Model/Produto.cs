﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace appNortao.Model
{
    public class Produto
    {
        [PrimaryKey]
        public int CodProduto { get; set; }
        public String Discriminacao { get; set; }
        public float PrecoUnitario { get; set; }
        public Double PrecoTotal { get; set; }
        public decimal Quantidade { get; set; }
        public String Descricao { get; set; }
    }
}
