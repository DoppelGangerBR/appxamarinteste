﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace appNortao.Model
{
    class Usuario
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public String usuario { get; set; }
        public String senha { get; set; }
        /*[MaxLength(600)]
        public String authtoken { get; set; }*/
    }

} 
