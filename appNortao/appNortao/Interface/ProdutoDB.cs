﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;
using appNortao.Model;
using appNortao.Controller;
namespace appNortao.Interface
{
    class ProdutoDB : IDisposable
    {
        private SQLiteConnection conexaoSQLite;
        private Logs logs;
        public ProdutoDB()
        {
            var config = DependencyService.Get<IConfig>();
            conexaoSQLite = new SQLiteConnection(Path.Combine(config.DiretorioSQLite, "baseApp.db3"));
            conexaoSQLite.CreateTable<Produto>();
        }
        public void InsereProduto(Produto produto)
        {
            conexaoSQLite.Insert(produto);
        }
        
        public void AtualizaProduto(Produto produto)
        {
            conexaoSQLite.Update(produto);
        }
        public void DeletaProduto(Produto produto)
        {
            conexaoSQLite.Delete(produto);
        }
        public Produto GetProduto(int codProduto)
        {
            try
            {
                return conexaoSQLite.Table<Produto>().FirstOrDefault(c => c.CodProduto == codProduto);
            }catch(Exception e)
            {
                logs.EscreveLog("Erro no metodo 'GetProduto' da classe 'ProdutoDB' | Descrição: " + e.Message);
                return null;
            }
        }
        public List<Produto> GetProdutos()
        {
            try
            {
                return conexaoSQLite.Table<Produto>().OrderBy(c => c.CodProduto).ToList();
            }
            catch (Exception e)
            {
                logs.EscreveLog("Erro no metodo 'GetProdutos' da classe 'ProdutoDB' | Descrição: " + e.Message);
                return null;
            }
        }
        public List<Produto> GetProdutoPorNome(string nomeProduto) {
            try {
                //return conexaoSQLite.Query<Produto>("SELECT * FROM produto where Discriminacao like \"%?%\";", nomeProduto);
                return conexaoSQLite.Table<Produto>().Where(i => i.Discriminacao == nomeProduto).ToList();
            }catch(Exception e) {
                return null;
            }
        }

        public void Dispose()
        {
            conexaoSQLite.Dispose();
        }
    }
}
