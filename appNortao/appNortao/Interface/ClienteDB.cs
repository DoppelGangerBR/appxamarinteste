﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using appNortao.Model;
using System.IO;

namespace appNortao.Interface
{
    
    class ClienteDB : IDisposable{
        private SQLiteConnection conexaoSQLite;

        public ClienteDB() {
            var config = DependencyService.Get<IConfig>();
            conexaoSQLite = new SQLiteConnection(Path.Combine(config.DiretorioSQLite, "baseApp.db3"));
            conexaoSQLite.CreateTable<Cliente>();
        }
        public void InsereCliente(Cliente cliente) {
            conexaoSQLite.Insert(cliente);
        }

        public void AtualizaCliente(Cliente cliente) {
            conexaoSQLite.Update(cliente);
        }
        public void DeletaCliente(Cliente cliente) {
            conexaoSQLite.Delete(cliente);
        }
        public Cliente GetCliente(int codCliente) {
            try {
                return conexaoSQLite.Table<Cliente>().FirstOrDefault(c => c.CodCliente == codCliente);
            }
            catch (Exception e) {
                return null;
            }
        }
        public List<Cliente> GetClientes() {
            try {
                return conexaoSQLite.Table<Cliente>().OrderBy(c => c.CodCliente).ToList();
            }
            catch (Exception e) {
                return null;
            }
        }
        public List<Cliente> GetClientePorNome(string nomeCliente) {
            try {
                return conexaoSQLite.Query<Cliente>("SELECT * FROM Cliente where Nome like \"%?%\"",nomeCliente);
            }catch(Exception e) {
                return null;
            }
        }
        public void Dispose() {
            conexaoSQLite.Dispose();
        }
    }
    
}
