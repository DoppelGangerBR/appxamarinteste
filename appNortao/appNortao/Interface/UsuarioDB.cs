﻿using appNortao.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using appNortao.Controller;
using SQLite;

namespace appNortao
{
    class UsuarioDB : IDisposable
    {
        private SQLiteConnection conexaoSQLite;
        public UsuarioDB()
        {
            var config = DependencyService.Get<IConfig>();
            conexaoSQLite = new SQLiteConnection(Path.Combine(config.DiretorioSQLite, "baseApp.db3"));
            Console.WriteLine("Criou o banco em ->" + conexaoSQLite);
            //Console.Write(config.Plataforma+Path.Combine(config.DiretorioSQLite, "baseApp"));
            conexaoSQLite.CreateTable<Usuario>();
        }

        public void InsereUsuario(Usuario usuario)
        {
            conexaoSQLite.Insert(usuario);
        }
        public void AtualizaUsuario(Usuario usuario)
        {
            conexaoSQLite.Update(usuario);
        }
        public void DeletaUsuario(Usuario usuario)
        {
            conexaoSQLite.Delete(usuario);
        }
        public Usuario GetUsuario(int idUsuario)
        {
            try
            {
                return conexaoSQLite.Table<Usuario>().FirstOrDefault(c => c.id == idUsuario);
            }catch(Exception e)
            {
                return null;
            }
            
        }
        public List<Usuario> GetUsuarios()
        {
            try
            {
                return conexaoSQLite.Table<Usuario>().OrderBy(c => c.usuario).ToList();
            }catch(Exception e)
            {
                return null;
            }
            
        }
        public Usuario GetUsuarioLogin(string login, string senha)
        {
            try
            {
                return conexaoSQLite.Table<Usuario>().FirstOrDefault(c => c.usuario == login && c.senha == senha);
            }catch(Exception e)
            {
                return null;
            }
        }

        public void Dispose()
        {
            conexaoSQLite.Dispose();
        }
    }
}
