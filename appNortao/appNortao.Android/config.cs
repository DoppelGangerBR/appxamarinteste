﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite.Net.Interop;
using Xamarin.Forms;

[assembly:Dependency(typeof(appNortao.Droid.config))]
namespace appNortao.Droid
{
    class config : IConfig
    {
        public string _diretorioSQLite;
        public SQLite.Net.Interop.ISQLitePlatform _plataforma;
        public string DiretorioSQLite
        {
            get
            {
                if (string.IsNullOrEmpty(_diretorioSQLite))
                {
                    //_diretorioSQLite = System.Environment.GetFolderPath(System.Environment.SpecialFolder, System.Environment.SpecialFolderOption.Create);
                    _diretorioSQLite = "/sdcard/Android/data/br.com.alfameta/files/";
                }
                return _diretorioSQLite;
            }
        }

        public ISQLitePlatform Plataforma
        {
            get
            {
                if(_plataforma == null)
                {
                    _plataforma = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
                }
                return _plataforma;
            }
        }
    }
}